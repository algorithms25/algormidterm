/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.algormidterm;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class AlgorMidterm {

    public static void main(String[] args) {
        Scanner n = new Scanner(System.in);
        int m = n.nextInt();
        int[ ] arr = new int[m];

        for (int i = 0; i < m; i++) {
            arr[i] = n.nextInt();
        }
        for (int j=m-1; j>=0; j--) {
            System.out.print(arr[j] + " ");
        }
    }
}
